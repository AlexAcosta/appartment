package com.inftel.appartment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.inftel.appartment.sqlite.FacturasSQLite;

public class FacturasEnBD extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facturas_en_bd);
		ListView mainListView = (ListView) findViewById(R.id.tvSQLinfo);
		FacturasSQLite info = new FacturasSQLite(this);
		info.open();
		ArrayList<String> data = info.leerDatos();
		info.close();

		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
				FacturasEnBD.this,
				android.R.layout.simple_list_item_1, data);

		mainListView.setAdapter(adaptador);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.facturas_en_bd, menu);
		return true;
	}

}

