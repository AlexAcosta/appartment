package com.inftel.appartment.model;

public class Usuario {
	
	private short id;
	private String token;
	private String email;
	private String nombre;
	private short idPiso;
	
	public Usuario () {
		
	}

	public Usuario(short id, String token, String email, String nombre,
			short idPiso) {
		this.id = id;
		this.token = token;
		this.email = email;
		this.nombre = nombre;
		this.idPiso = idPiso;
	}

	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public short getIdPiso() {
		return idPiso;
	}

	public void setIdPiso(short idPiso) {
		this.idPiso = idPiso;
	}
	
}
