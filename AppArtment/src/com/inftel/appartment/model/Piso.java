package com.inftel.appartment.model;

public class Piso {
	private String nombre;
	private String direccion;
	private short id;
	
	public Piso () {
		
	}
	
	public Piso(String nombre, String direccion, short id) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public short getId() {
		return id;
	}
	public void setId(short id) {
		this.id = id;
	}
}
