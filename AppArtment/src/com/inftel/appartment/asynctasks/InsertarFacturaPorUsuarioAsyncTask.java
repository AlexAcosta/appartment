package com.inftel.appartment.asynctasks;

import org.json.JSONObject;

import android.os.AsyncTask;

import com.inftel.appartment.conexiones.InsertarFacturaPorUsuarioConnection;

public class InsertarFacturaPorUsuarioAsyncTask extends AsyncTask<JSONObject,Integer,Void> {

	@Override
	protected Void doInBackground(JSONObject... dato) {
		InsertarFacturaPorUsuarioConnection.InsertarFacturaPorUsuario(dato[0]);
		return null;
	}

}
