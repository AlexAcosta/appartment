package com.inftel.appartment.asynctasks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.inftel.appartment.FacturasActivity;
import com.inftel.appartment.FacturasLVAdapter;
import com.inftel.appartment.MostrarDetallesFacturaActivity;
import com.inftel.appartment.R;
import com.inftel.appartment.conexiones.ObtenerFacturasConnection;
import com.inftel.appartment.sqlite.FacturasSQLite;

public class ObtenerFacturasAsyncTask extends AsyncTask<String, Void, JSONArray> {

	ProgressDialog dialog;
	FacturasActivity c;

	public ObtenerFacturasAsyncTask (FacturasActivity c){
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Cargando facturas...");
		dialog.setIndeterminate(true);
		dialog.show();
	}



	@Override
	protected JSONArray doInBackground(String... params) {
		return ObtenerFacturasConnection.ObtenerFacturas();
	}

	@Override
	protected void onPostExecute (JSONArray respJSON){
		JSONArray facturasJSON = new JSONArray();
		ListView lstFacturas = (ListView) c.findViewById(R.id.lstFacturas);
		for (int i = 0; i < respJSON.length(); i++) {
			JSONObject usuarioPorFactura;
			try {
				usuarioPorFactura = respJSON.getJSONObject(i);
				JSONObject factura = usuarioPorFactura.getJSONObject("idFactura");
				int idFac = factura.getInt("id");
				String fechaFac = factura.getString("fecha");
				String notasFac = new String();
				if (factura.has("notas")){
					notasFac = factura.getString("notas");
				}
				String conceptoFac = factura.getString("concepto");
				int cantidadFac = factura.getInt("cantidad");
				int pagadaFac = factura.getInt("pagada");

				JSONObject piso = usuarioPorFactura.getJSONObject("idPiso");
				int idPiso = piso.getInt("id");

				// ------------ INSERCION DE ESTAS FACTURAS EN LA BASE DE DATOS----------

				FacturasSQLite entry = new FacturasSQLite(c);
				entry.open();
				entry.insertarFactura(idFac, conceptoFac, cantidadFac, fechaFac, notasFac, pagadaFac, idPiso, 0);
				entry.close();

				// ----------------------------------------------------------------------

				JSONObject facOrdenar = new JSONObject();
				facOrdenar.put("conceptoFac", conceptoFac);
				facOrdenar.put("cantidadFac", cantidadFac);
				facOrdenar.put("pagada", pagadaFac);
				facOrdenar.put("idFac", idFac);
				facturasJSON.put(facOrdenar);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// Ordenamos las facturas. Primero las no pagadas.
		facturasJSON = ordenarFacturas(facturasJSON);
		lstFacturas.setAdapter(new FacturasLVAdapter(c, facturasJSON));
		lstFacturas.setOnItemClickListener(new myOnClickListener(facturasJSON));
		if (dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	private class myOnClickListener implements OnItemClickListener {

		private JSONArray facturas;

		public myOnClickListener(JSONArray facturas) {
			this.facturas = facturas;
		}

		@Override
		public void onItemClick(AdapterView<?> adapter, View v, int pos,
				long arg3) {
			Intent i = new Intent(c, MostrarDetallesFacturaActivity.class);
			try {
				i.putExtra("idFactura", facturas.getJSONObject(pos).getInt("idFac"));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			c.startActivity(i);
		}
	}

	public JSONArray ordenarFacturas(JSONArray a) {
		JSONArray devolver = new JSONArray();
		try {
			for (int i = 0; i < a.length(); i++) {
				if (a.getJSONObject(i).getInt("pagada") == 0) {
					devolver.put(a.get(i));
				}
			}
			for (int i = 0; i < a.length(); i++) {
				if (a.getJSONObject(i).getInt("pagada") == 1) {
					devolver.put(a.get(i));
				}
			}

		} catch (JSONException e) {

		}
		return devolver;
	}
}