package com.inftel.appartment.asynctasks;

import org.json.JSONObject;

import android.os.AsyncTask;

import com.inftel.appartment.conexiones.InvitarUsuarioConnection;

public class InvitarUsuarioAsyncTask extends AsyncTask<JSONObject, Void, Void> {
	@Override
	protected Void doInBackground(JSONObject... arg0) {
		InvitarUsuarioConnection.invitarUsuario(arg0[0]);
		return null;
	}
}


