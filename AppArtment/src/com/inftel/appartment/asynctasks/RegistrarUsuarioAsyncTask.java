package com.inftel.appartment.asynctasks;

import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;

import com.inftel.appartment.FacturasActivity;
import com.inftel.appartment.LoginActivity;
import com.inftel.appartment.NoPisoActivity;
import com.inftel.appartment.conexiones.RegistrarUsuarioConnection;
import com.inftel.appartment.singleton.UsuarioSingleton;

public class RegistrarUsuarioAsyncTask extends AsyncTask<String, Void, JSONObject> {

	ProgressDialog dialog;
	LoginActivity c;

	public RegistrarUsuarioAsyncTask (LoginActivity c){
		this.c = c;
		dialog = new ProgressDialog(c);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setTitle("Progreso");
		dialog.setMessage("Cargando entradas...");
		dialog.setIndeterminate(true);
		dialog.show();
	}

	@Override
	protected JSONObject doInBackground(String... arg0) {
		return RegistrarUsuarioConnection.registrarUsuario(arg0[0], arg0[1]);
	}

	protected void onPostExecute(JSONObject result){
		super.onPostExecute(result);
		JSONArray roommates = new JSONArray();
		UsuarioSingleton us = UsuarioSingleton.getInstance();
		us = UsuarioSingleton.getInstance();
		us.init(result);
		if (result.has("idPiso"))
		{
			try {
				roommates=new RoommatesAsyncTask().execute(result.getString("email")).get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			us.init(roommates);
		}



		if (dialog.isShowing()) {
			dialog.dismiss();
		}

		if (us.getPiso() != null){
			Intent mainActivity = new Intent(c, FacturasActivity.class);
			c.startActivity(mainActivity);
		}

		else{
			Intent noPiso = new Intent(c, NoPisoActivity.class);
			c.startActivity(noPiso);
		}
	}
}
