package com.inftel.appartment.utilities;

public class StaticResources {
    public static final String ip = "http://192.168.1.141";
    public static final String puerto = "52744";
    public static final int connection_timeout = 10000;
    public static final int datatrieval_timeout = 10000;
    public static final String url_path_server="/appartment-webservices/webresources/appartment.entities.";
    public static final String url_autenticar_usuario = url_path_server+"usuario/registrar?email=";
    public static final String url_terminar_sesion = url_path_server+"usuario/invalidarToken?token=";
    public static final String title_activity_facturas_en_bd = "FacturasEnBD";
    public static final String url_fetch_usuario = url_path_server+"usuario/find";
    public static final String url_facturasporpiso = url_path_server+"usuarioporfactura/IdPiso=";
    public static final String url_crear_piso = url_path_server+"piso";
    public static final String shared_preferences_filename = "com.inftel.appartment.PREFERENCE_FILE_KEY";
    public static final String url_roommates = url_path_server+"usuario/email=";
    public static final String url_insertar_factura=url_path_server+"factura/factura";
    public static final String url_insertar_facturas_por_usuario=url_path_server+"usuarioporfactura";
    public static final String url_token = "&token=";
    public static final String url_piso = "&piso="; 
    public static final String url_email = "&email="; 
    public static final String url_obtener_facturas_por_usuario=url_path_server+"usuarioporfactura/obtenerFactura=";
    public static final String url_modificar_factura=url_path_server+"usuarioporfactura/editar";
    public static final String url_actualizar_factura=url_path_server+"factura";
    public static final String url_invitar = url_path_server+"usuario/invitar";
}
