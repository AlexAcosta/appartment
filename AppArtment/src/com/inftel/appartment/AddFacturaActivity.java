package com.inftel.appartment;




import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.inftel.appartment.asynctasks.InsertarFacturasAsyncTask;
import com.inftel.appartment.singleton.UsuarioSingleton;


public class AddFacturaActivity extends Activity {

	private Button btnInsertar;
	private EditText txtCantidad;
	private EditText txtConcepto;
	private EditText txtNota;
	private DatePicker date_picker;

	private int year;
	private int month;
	private int day;


	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_factura);

		btnInsertar = (Button)findViewById(R.id.btnInsertar);
		txtCantidad = (EditText)findViewById(R.id.txtCantidad);
		txtConcepto = (EditText)findViewById(R.id.txtConcepto);
		date_picker = (DatePicker)findViewById(R.id.txtFecha);
		txtNota = (EditText)findViewById(R.id.txtNota);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		Calendar calendar = Calendar.getInstance();

		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		day = calendar.get(Calendar.DAY_OF_MONTH);

		date_picker.init(year, month, day, null);

		btnInsertar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String cantidad = txtCantidad.getText().toString();
				String concepto = txtConcepto.getText().toString();
				if (cantidad.length() != 0 && concepto.length() != 0){
					UsuarioSingleton u= UsuarioSingleton.getInstance();
					InsertarFacturasAsyncTask tarea = new InsertarFacturasAsyncTask(AddFacturaActivity.this);
					JSONObject dato = new JSONObject();
					date_picker = (DatePicker)findViewById(R.id.txtFecha);

					try {
						JSONObject aux = new JSONObject();
						dato.put("cantidad", Integer.parseInt(txtCantidad.getText().toString()));

						Calendar d = new GregorianCalendar(Integer.parseInt(Integer.toString(date_picker.getYear())),
								Integer.parseInt(Integer.toString(date_picker.getMonth())), Integer.parseInt(Integer.toString(date_picker.getDayOfMonth())));

						dato.put("notas", txtNota.getText().toString());
						dato.put("pagada", 0);
						aux.put("id", u.getPiso().getId());
						dato.put("idPiso", aux);
						dato.put("fecha", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(d.getTime()));			
						dato.put("concepto", txtConcepto.getText().toString());

						tarea.execute(dato);


					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				else{
					Toast.makeText(getApplicationContext(), "Faltan campos por rellenar", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_factura, menu);
		return true;
	}






}