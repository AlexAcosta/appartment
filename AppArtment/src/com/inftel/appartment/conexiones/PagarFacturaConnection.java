package com.inftel.appartment.conexiones;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.utilities.StaticResources;

public class PagarFacturaConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_MODIFICAR_FACTURA = StaticResources.url_modificar_factura;

	public static JSONArray pagarFactura(JSONObject dato) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost post = new HttpPost(IP + ":" + PUERTO + URL_MODIFICAR_FACTURA);

		UsuarioSingleton u = UsuarioSingleton.getInstance();
		post.addHeader("token", u.getUsuario().getToken());

		post.setHeader("content-type", "application/json");
		StringEntity entity;
		try {

			entity = new StringEntity(dato.toString());
			post.setEntity(entity);
			HttpResponse resp = httpClient.execute(post);
			String respStr = EntityUtils.toString(resp.getEntity());
			return new JSONArray(respStr);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

}
