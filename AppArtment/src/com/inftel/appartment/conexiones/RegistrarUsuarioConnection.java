package com.inftel.appartment.conexiones;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.inftel.appartment.utilities.StaticResources;
import com.inftel.appartment.utilities.Utilities;

public class RegistrarUsuarioConnection {

	private static int CONNECTION_TIMEOUT = StaticResources.connection_timeout;
	private static int DATATRIEVAL_TIMEOUT = StaticResources.datatrieval_timeout;
	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_AUTENTICAR_USUARIO = StaticResources.url_autenticar_usuario;

	public static JSONObject registrarUsuario(String email, String nombre) {
		HttpURLConnection urlConnection = null;
		try {
			URL urlToRequest = new URL(IP + ":" + PUERTO
					+ URL_AUTENTICAR_USUARIO + email + "&nombre=" + nombre);
			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
			urlConnection.setReadTimeout(DATATRIEVAL_TIMEOUT);

			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				Log.e("Error 403", "Acceso no autorizado");
			} else if (statusCode != HttpURLConnection.HTTP_OK) {
				Log.e("Error: ", String.valueOf(statusCode));
			}
			InputStream in = urlConnection.getInputStream();
			return new JSONObject(Utilities.getResponseText(in));

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return null;
	}
}
