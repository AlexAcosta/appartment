package com.inftel.appartment.conexiones;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.utilities.StaticResources;

public class InvitarUsuarioConnection {

	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_INVITAR_PISO = StaticResources.url_invitar;

	public static void invitarUsuario(JSONObject dato) {
		HttpClient httpClient = new DefaultHttpClient();

		HttpPost post = new HttpPost(IP + ":" + PUERTO + URL_INVITAR_PISO);
		UsuarioSingleton u = UsuarioSingleton.getInstance();
		post.addHeader("token", u.getUsuario().getToken());

		post.setHeader("content-type", "application/json");
		StringEntity entity;
		try {
			entity = new StringEntity(dato.toString());
			post.setEntity(entity);
			httpClient.execute(post);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
