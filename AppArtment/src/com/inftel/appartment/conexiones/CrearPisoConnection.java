package com.inftel.appartment.conexiones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.inftel.appartment.singleton.UsuarioSingleton;
import com.inftel.appartment.utilities.StaticResources;

public class CrearPisoConnection {
	
	private static int CONNECTION_TIMEOUT = StaticResources.connection_timeout;
	private static int DATATRIEVAL_TIMEOUT = StaticResources.datatrieval_timeout;
	private static String IP = StaticResources.ip;
	private static String PUERTO = StaticResources.puerto;
	private static String URL_CREAR_PISO = StaticResources.url_crear_piso;

	public static JSONObject crearPiso (String nombre, String direccion){
		HttpURLConnection urlConnection = null;
		String line;
		StringBuffer jsonString = new StringBuffer();
		UsuarioSingleton u = UsuarioSingleton.getInstance();
		JSONObject piso = new JSONObject();
		try {
			piso.put("nombre", nombre);
			piso.put("direccion", direccion);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		try {
			// Creamos la conexion
			URL urlToRequest = new URL(IP+":"+PUERTO+URL_CREAR_PISO);
			urlConnection = (HttpURLConnection) urlToRequest.openConnection();
			urlConnection.setRequestMethod("POST");
			urlConnection.setDoOutput(true);
			urlConnection.setDoInput(true);
			urlConnection.setRequestProperty("Accept", "application/json");
	        urlConnection.setRequestProperty("Content-Type", "application/json");
	        urlConnection.setRequestProperty("tokencreate", u.getUsuario().getToken());
			urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
			urlConnection.setReadTimeout(DATATRIEVAL_TIMEOUT);
			urlConnection.connect();
			
			//Anyadimos la informacion del piso 
			OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8");
	        writer.write(piso.toString());
	        writer.close();

			// Manejar errores
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				Log.e("Error 403", "Acceso no autorizado");
			} else if (statusCode != HttpURLConnection.HTTP_OK) {
				Log.e("Error: ", String.valueOf(statusCode));
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	        while ((line = br.readLine()) != null) {
	                jsonString.append(line);
	        }
	        br.close();
	        urlConnection.disconnect();
			return new JSONObject(jsonString.toString());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return null;
	}
}
