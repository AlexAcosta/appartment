package com.inftel.appartment;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FacturasLVAdapter extends BaseAdapter {

	Context context;
	JSONArray data;

	private static LayoutInflater inflater = null;

	public FacturasLVAdapter(Context c, JSONArray data){
		this.context = c;
		this.data = data;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	@Override
	public int getCount() {
		return data.length();
	}

	@Override
	public String getItem(int i) {
		try {
			return data.getJSONObject(i).getString("idFac");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		try{
			if (vi == null)
				vi = inflater.inflate(R.layout.row, null);
			TextView concepto = (TextView) vi.findViewById(R.id.concepto);
			TextView cantidad = (TextView) vi.findViewById(R.id.cantidad);
			concepto.setText(data.getJSONObject(position).getString("conceptoFac"));
			cantidad.setText(data.getJSONObject(position).getString("cantidadFac"));
			if (data.getJSONObject(position).getInt("pagada") == 0){
				ImageView im = (ImageView) vi.findViewById(R.id.tick);
				im.setImageResource(R.drawable.cross);
			}
			else{
				ImageView im = (ImageView) vi.findViewById(R.id.tick);
				im.setImageResource(R.drawable.tick);
			}
		}
		catch (JSONException e){
			e.printStackTrace();
		}
		return vi;
	}

}
